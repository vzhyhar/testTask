package com.mbicycle.farm.iface;

public interface Farm<T> {
    T giveBirth(int parentId, int childId, String nickName);
    T endLifeSpan(int id);
    void printFarmData();
}
