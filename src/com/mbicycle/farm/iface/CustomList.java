package com.mbicycle.farm.iface;

public interface CustomList<E> {
    void add(E element);
    boolean remove(E element);
    boolean contains(E element);
    int size();
    String print();
    E get(E element);
}
