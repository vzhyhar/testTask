package com.mbicycle.farm;

import com.mbicycle.farm.entity.Cow;
import com.mbicycle.farm.iface.Farm;
import com.mbicycle.farm.impl.CowFarmCustomListImpl;
import com.mbicycle.farm.impl.CowFarmMapImpl;
import com.mbicycle.farm.impl.CowFarmSetImpl;

public class Runner {
    public static void main(String[] args) {

        Farm<Cow> cowFarm = new CowFarmSetImpl();
        cowFarm.giveBirth(0, 1, "bbb");
        cowFarm.giveBirth(1, 2, "ccc");
        cowFarm.giveBirth(1, 3, "bbb");
        cowFarm.giveBirth(3, 4, "bbb");
        cowFarm.giveBirth(0, 5, "bbb");
        cowFarm.endLifeSpan(3);
        cowFarm.printFarmData();

        Farm<Cow> cowFarmMap = new CowFarmMapImpl();
        cowFarmMap.giveBirth(0, 1, "bbb");
        cowFarmMap.giveBirth(1, 2, "ccc");
        cowFarmMap.giveBirth(1, 3, "bbb");
        cowFarmMap.giveBirth(3, 4, "bbb");
        cowFarmMap.giveBirth(0, 5, "bbb");
        cowFarmMap.endLifeSpan(3);
        cowFarmMap.printFarmData();

        Farm<Cow> farm = new CowFarmCustomListImpl();
        farm.giveBirth(0, 1, "bbb");
        farm.giveBirth(1, 2, "ccc");
        farm.giveBirth(1, 3, "bbb");
        farm.giveBirth(3, 4, "bbb");
        farm.giveBirth(0, 5, "bbb");
        farm.endLifeSpan(3);
        farm.printFarmData();

    }
}
