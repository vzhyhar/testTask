package com.mbicycle.farm.entity;

public class Cow {
    private int cowId;
    private String nickName;

    public Cow() {
    }

    public Cow(int cowId, String nickName) {
        this.cowId = cowId;
        this.nickName = nickName;
    }

    public int getCowId() {
        return cowId;
    }

    public void setCowId(int cowId) {
        this.cowId = cowId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Cow birth(int childId, String childNickName) {
        return new Cow(childId, childNickName);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cow cow = (Cow) o;
        return cowId == cow.cowId;
    }

    @Override
    public int hashCode() {
        return cowId;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder()
                .append("Cow: cowId=")
                .append(cowId)
                .append(", nickName='")
                .append(nickName)
                .append("\'\n");
        return  builder.toString();
    }
}
