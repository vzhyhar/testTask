package com.mbicycle.farm.constants;

public class Constants {
    private Constants() {}
    public static final String ERR_UNABLE_TO_ADD = "Unable to add cow, parent with id %d does not exists";
    public static final String ERR_UNABLE_TO_DELETE = "Unable to delete cow, cow with id %d not found";
    public static final String ERR_ALREADY_EXISTS = "Cow with id %d already exists";
}
