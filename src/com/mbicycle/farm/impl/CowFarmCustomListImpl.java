package com.mbicycle.farm.impl;

import com.mbicycle.farm.constants.Constants;
import com.mbicycle.farm.entity.Cow;
import com.mbicycle.farm.iface.CustomList;
import com.mbicycle.farm.iface.Farm;

public class CowFarmCustomListImpl implements Farm<Cow> {
    private CustomList<Cow> cows;

    public CowFarmCustomListImpl() {
        cows = new CustomListImpl<>();
        cows.add(new Cow(0, "zzz"));
    }

    @Override
    public Cow giveBirth(int parentId, int childId, String nickName) {
       Cow parent = cows.get(new Cow(parentId, null));
       if (parent == null) {
           String errorMessage = String.format(Constants.ERR_UNABLE_TO_ADD, parentId);
           throw new IllegalArgumentException(errorMessage);
       }
       Cow child = parent.birth(childId, nickName);
       if (cows.contains(child)) {
           String errorMessage = String.format(Constants.ERR_ALREADY_EXISTS, childId);
           throw new IllegalArgumentException(errorMessage);
       }
        cows.add(child);
       return child;
    }

    @Override
    public Cow endLifeSpan(int id) {
        Cow dying = cows.get(new Cow(id, null));
        if (dying == null) {
            String errorMessage = String.format(Constants.ERR_UNABLE_TO_DELETE, id);
            throw new IllegalArgumentException(errorMessage);
        }
        cows.remove(dying);
        return dying;
    }

    @Override
    public void printFarmData() {
        System.out.print(cows.print());
        System.out.println("Total cows count: " + cows.size() + "\n");
    }
}
