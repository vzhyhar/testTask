package com.mbicycle.farm.impl;

import com.mbicycle.farm.constants.Constants;
import com.mbicycle.farm.entity.Cow;
import com.mbicycle.farm.iface.Farm;

import java.util.HashSet;
import java.util.Set;

public class CowFarmSetImpl implements Farm<Cow> {
    private Set<Cow> cows;

    public CowFarmSetImpl() {
        this.cows = new HashSet<>();
        cows.add(new Cow(0, "aaa"));
    }

    @Override
    public Cow giveBirth(int parentId, int childId, String nickName) {
        Cow parent = findCowById(parentId);
        if (parent == null) {
            String errorMessage = String.format(Constants.ERR_UNABLE_TO_ADD, parentId);
            throw new IllegalArgumentException(errorMessage);
        }
        if (cows.contains(findCowById(childId))) {
            String errorMessage = String.format(Constants.ERR_ALREADY_EXISTS, childId);
            throw new IllegalArgumentException(errorMessage);
        }
        Cow child = parent.birth(childId, nickName);
        cows.add(child);
        return child;
    }

    @Override
    public Cow endLifeSpan(int cowId) {
        Cow cow = findCowById(cowId);
        if (cow == null) {
            String errorMessage = String.format(Constants.ERR_UNABLE_TO_DELETE, cowId);
            throw new IllegalArgumentException(errorMessage);
        }
        cows.remove(cow);
        return cow;
    }

    @Override
    public void printFarmData() {
        System.out.println(getFarmData());
    }

    private Cow findCowById(int cowId) {
        return cows.stream()
                .filter(cow -> cow.getCowId() == cowId)
                .findFirst()
                .orElse(null);
    }

    private String getFarmData() {
        StringBuilder builder = new StringBuilder();
        cows.forEach(builder::append);
        builder.append("Total cows count: ")
                .append(cows.size())
                .append("\n");
        return builder.toString();
    }
}

