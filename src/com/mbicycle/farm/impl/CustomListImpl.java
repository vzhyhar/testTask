package com.mbicycle.farm.impl;

import com.mbicycle.farm.iface.CustomList;

public class CustomListImpl<E> implements CustomList<E> {
    private Node<E> head;
    private Node<E> tail;
    private int size = 0;

    class Node<E> {
        Node previous;
        Node next;
        E value;
        Node(E value) {
            this.value = value;
        }
    }

    @Override
    public void add(E element) {
        if (element == null) {
            throw new IllegalArgumentException("Addition of null elements to the list is forbidden");
        }
        Node<E> node = new Node<>(element);
        if (head == null) {
            head = node;
            head.next = tail;
        } else {
            if (head.next == null) {
                node.previous = head;
                head.next = node;
                tail = node;
            } else {
                tail.next = node;
                node.previous = tail;
                tail = node;
            }
        }
        size++;
    }

    @Override
    public boolean remove(E element) {
        if (element == null) {
            return false;
        }
        for (Node<E> node = head; node != null; node = node.next) {
            if (element.equals(node.value)) {
                Node <E> prev = node.previous;
                Node<E> next = node.next;
                if (prev == null) {
                    head = next;
                } else {
                    prev.next = next;
                    prev = null;
                }
                if (next == null) {
                    tail = prev;
                } else {
                    next.previous = prev;
                    next = null;
                }
                node.value = null;
                size--;
                return true;
            }
        }
        return false;
    }


    public boolean contains(E element) {
        return indexOf(element) != -1;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public String print() {
        Node<E> current = head;
        StringBuilder builder = new StringBuilder();
        while (current != null) {
            builder.append(current.value);
            current = current.next;
        }
        return builder.toString();
    }

    @Override
    public E get(E element) {
        if (element == null) {
            return null;
        }
        Node<E> current = head;
        while (current != null) {
            if (element.equals(current.value)) {
                return current.value;
            }
            current = current.next;
        }
        return null;
    }


    private int indexOf(E element) {
        if (element == null) {
            return -1;
        }
        int index = 0;
        Node<E> node = head;
        while (node != null) {
            if (element.equals(node.value)) {
                return index;
            }
            node = node.next;
            index++;
        }
        return -1;
    }
}
