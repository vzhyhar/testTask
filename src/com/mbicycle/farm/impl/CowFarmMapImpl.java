package com.mbicycle.farm.impl;

import com.mbicycle.farm.constants.Constants;
import com.mbicycle.farm.entity.Cow;
import com.mbicycle.farm.iface.Farm;

import java.util.HashMap;
import java.util.Map;

public class CowFarmMapImpl implements Farm<Cow> {
    private Map<Integer, Cow> cows;

    public CowFarmMapImpl() {
        cows = new HashMap<>();
        cows.put(0, new Cow(0, "aaa"));
    }

    @Override
    public Cow giveBirth(int parentId, int childId, String nickName) {
        Cow parent = cows.get(parentId);
        if (parent == null) {
            String errorMessage = String.format(Constants.ERR_UNABLE_TO_ADD, parentId);
            throw new IllegalArgumentException(errorMessage);
        }
        if (cows.containsKey(childId)) {
            String errorMessage = String.format(Constants.ERR_ALREADY_EXISTS, childId);
            throw new IllegalArgumentException(errorMessage);
        }
        Cow child = parent.birth(childId, nickName);
        cows.put(childId, child);
        return child;
    }

    @Override
    public Cow endLifeSpan(int id) {
        Cow cow = cows.get(id);
        if (cow != null) {
            cows.remove(id);
        } else {
            String errorMessage = String.format(Constants.ERR_UNABLE_TO_DELETE, id);
            throw new IllegalArgumentException(errorMessage);
        }
        return cow;
    }

    @Override
    public void printFarmData() {
        System.out.println(getFarmData());
    }

    private String getFarmData() {
        StringBuilder builder = new StringBuilder();
        cows.values().forEach(builder::append);
        builder.append("Total cows count: ")
                .append(cows.size())
                .append("\n");
        return builder.toString();
    }
}
